﻿using System;
using System.IO;
using System.Xml.Linq;
using ASVG;

namespace ASVGReader
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine(">>Enter xml path:");
            string path = Console.ReadLine();
            IIntentionTree tree = new IntentionTree(File.ReadAllText(path).Replace("\r\n", "\n"));

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Parents:");
                Console.WriteLine(tree.CurrentNode.ToString());
                Console.WriteLine();
                Console.WriteLine("Tree:");
                Console.WriteLine(tree.CurrentNode.PathToLeafs(0));
                Console.WriteLine();
                Console.WriteLine("Graphic elements:");
                foreach (XElement graphicXmlElement in tree.CurrentNode.GraphicsChildsFromSVG)
                {
                        Console.WriteLine(graphicXmlElement.Name.LocalName + ": ID=" +
                                          graphicXmlElement.Attribute("id").Value +
                                          ", Value=" + graphicXmlElement.Attribute("value").Value);
                }
                Console.WriteLine();
                Console.WriteLine("What do you want to do next?");
                Console.WriteLine("1 ... x: Select child #i");
                Console.WriteLine("U: Go up");
                Console.WriteLine("X: Exit");
                Console.Write(">> ");
                string input = Console.ReadLine().ToUpper();
                int numberInput = -1;
                Int32.TryParse(input, out numberInput);

                if (numberInput > 0)
                {
                    tree.GoDown(numberInput-1);
                }
                else
                {
                    switch (input)
                    {
                        case "U":
                            tree.GoUp();
                            break;
                        case "X":
                            Console.WriteLine("Good bye!");
                            return;
                        default:
                            Console.WriteLine("Input unknown!");
                            break;
                    }
                }
            }
        }
    }
}