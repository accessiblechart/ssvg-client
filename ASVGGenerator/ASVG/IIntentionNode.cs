﻿using System.Collections.Generic;
using System.Xml.Linq;
using ASVG;

namespace ASVG
{
    /// <summary>
    /// This class represents a node in the intention tree
    /// </summary>
    public interface IIntentionNode
    {
        /// <summary>
        /// The tree this node belongs to
        /// </summary>
        IIntentionTree Tree { get; set; }
        /// <summary>
        /// The type of this node.
        /// This typically is the axis-label (like Year, State, Party, etc.)
        /// </summary>
        string Type { get; }
        /// <summary>
        /// The node value.
        /// This is typically one value on an axis (like "1994", "Upper Austria", etc.)
        /// </summary>
        string Value { get; }
        /// <summary>
        /// Defines how the node can caluclate its value based on its children.
        /// If this is not possible, "None" should be selected as aggregation type.
        /// </summary>
        AggregationType AggregationType { get; }
        /// <summary>
        /// The child nodes
        /// </summary>
        List<IIntentionNode> Children { get; }
        /// <summary>
        /// The parent node.
        /// Null for the root node.
        /// </summary>
        IIntentionNode Parent { get; set; }
        /// <summary>
        /// The Nodes' String-representation
        /// </summary>
        /// <returns></returns>
        string ToString(); //Returns PathFromRoot
        /// <summary>
        /// The 'graphic'-nodes below this node. (Not just in child level, also in grandchild, ...)
        /// </summary>
        IList<IIntentionNode> GraphicChilds { get; }
        /// <summary>
        /// The 'graphic'-nodes XML-node-representation in the SVG (like rect, ...)
        /// </summary>
        IList<XElement> GraphicsChildsFromSVG { get; }
        /// <summary>
        /// The ids of the 'graphic'-nodes below this node (Not just in child level, also in grandchild, ...)
        /// </summary>
        IList<string> GraphicChildIds { get; }
        /// <summary>
        /// The calculated value based on the Aggregation-type.
        /// Null / string.Empty if no value can be calculated (eg. multiple graphic childs without aggegation mode set)
        /// </summary>
        string CalculatedValue { get; }
        /// <summary>
        /// A String-representation of the path from root to this node
        /// </summary>
        string PathFromRoot { get; }
        /// <summary>
        /// A short String-representation of the path from root to this node.
        /// Should be usable for text-to-speech.
        /// </summary>
        string PathFromRootShort { get; }
        /// <summary>
        /// Converts this intention node to an XML.
        /// Can be used for export.
        /// </summary>
        XElement AsXml { get; }
        /// <summary>
        /// Combines the calculated value and the aggregation type to one string.
        /// </summary>
        string CalculatedValueFullText { get; }
        /// <summary>
        /// Recursive function to generate a string-representation for the path from the current node to its leafs
        /// </summary>
        /// <param name="level">Used to determine how much indentation is needed.</param>
        /// <returns>A string representation of the path from this node to its leafs</returns>
        string PathToLeafs(int level); //Recursive methods, call with level=0
        /// <summary>
        /// Adds a child node to this node
        /// </summary>
        /// <param name="intentionNode">The node to add</param>
        /// <returns>The added node with updated parent property</returns>
        IIntentionNode AddChild(IIntentionNode intentionNode);
        /// <summary>
        /// Gets the specified child
        /// </summary>
        /// <param name="i">The childs ID (beginning with 0)</param>
        /// <returns>The child if it exits, otherwise null</returns>
        IIntentionNode GetChild(int i); // 0 ... n
    }
}
