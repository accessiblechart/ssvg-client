﻿namespace ASVG
{
    /// <summary>
    /// Defining how a node can compute its value based on its child nodes.
    /// If node cannot be calculated based on its child nodes, value "None" should be selected.
    /// </summary>
    public enum AggregationType
    {
        Add, Average, None
    }
}
