﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace ASVG
{
    public interface IIntentionTree
    {
        /// <summary>
        /// The SVGs root node the intention tree belongs to
        /// </summary>
        XElement SvgXmlRoot { get; }
        /// <summary>
        /// The intention trees root node
        /// </summary>
        IIntentionNode RootNode { get; }
        /// <summary>
        /// The current node.
        /// This gets changed by navigating the tree.
        /// </summary>
        IIntentionNode CurrentNode { get; }
        /// <summary>
        /// The last node a comparism was made to
        /// </summary>
        IIntentionNode LastCompareNode { get; }
        /// <summary>
        /// Go up in the tree.
        /// </summary>
        void GoUp();
        /// <summary>
        /// Go down into a child node.
        /// </summary>
        /// <param name="child">The child node number</param>
        void GoDown(int child); //0 ... n
        /// <summary>
        /// Compare on a certain level. LastCompareNode will be set and a comparism text will be returned.
        /// </summary>
        /// <param name="level">The level on which the comparism should happen. 0 = siblings, 1 = cousin, etc.</param>
        /// <returns>A textual representation of the comparism</returns>
        string Compare(int level); //Sets LastCompareNode
        /// <summary>
        /// Stay on the same level and select the next node (sibling).
        /// </summary>
        /// <returns>The selected node</returns>
        IIntentionNode NextNode();
        /// <summary>
        /// Stay on the same level and select the previous node (sibling).
        /// </summary>
        /// <returns>The selected node</returns>
        IIntentionNode PreviousNode();
        /// <summary>
        /// Get all nodes of a certain type with a certain value.
        /// If no value is given (null), all nodes of a certain type are returned.
        /// </summary>
        /// <param name="type">The type</param>
        /// <param name="value">The value</param>
        /// <returns>All nodes of a certain type. If value is given, only nodes of the given type with the given value are returned.</returns>
        IEnumerable<IIntentionNode> GetNodes(string type, string value);

        /// <summary>
        /// The textual representation for the last compare.
        /// </summary>
        string LastCompareResult { get; }
        /// <summary>
        /// The short representation for the last compare.
        /// This should be compatible to text-to-speech.
        /// </summary>
        string LastCompareResultShort { get; }
    }
}
