﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ASVG;

namespace ASVG
{
    public class IntentionTree : IIntentionTree
    {
        private int[] compareIDs = new int[10];

        public IntentionTree(string svgCode)
        {
            XElement fileRoot = XElement.Parse(svgCode);
            SvgXmlRoot = fileRoot;
            RootNode =
                ASVGGenerator.ConvertXMLToSNT(
                    (from XElement ele in fileRoot.DescendantsAndSelf()
                        where ele.Name.LocalName == "intentionTree"
                        select ele)
                        .Single(), null, this);

            CurrentNode = RootNode;

            LastCompareNode = null;
        }

        public IntentionTree(string csv, string options)
        {
            SvgXmlRoot = null;

            ASVGGenerator gen = new ASVGGenerator(csv, options);
            RootNode = gen.GenerateBasicTree(this);

            CurrentNode = RootNode;

            LastCompareNode = null;
        }

        public XElement SvgXmlRoot { get; private set; }
        public IIntentionNode RootNode { get; private set; }
        public IIntentionNode CurrentNode { get; private set; }
        public IIntentionNode LastCompareNode { get; private set; }

        public void GoUp()
        {
            ResetCompareNodes();

            IIntentionNode parent = CurrentNode.Parent;
            if (parent != null)
            {
                CurrentNode = parent;
                Console.WriteLine("Parent selected!");
            }
            else
            {
                Console.WriteLine("Parent does not exist (must be root node)!");
            }
        }

        public void GoDown(int childNr)
        {
            ResetCompareNodes();

            IIntentionNode child = CurrentNode.GetChild(childNr);
            if (child != null && child.Type != "graphic")
            {
                CurrentNode = child;
                LastCompareNode = null;
                Console.WriteLine("Child selected!");
            }
            else
            {
                Console.WriteLine("Child " + childNr + " does not exist!");
            }
        }

        public string Compare(int level)
        {
            if (CurrentNode.CalculatedValue.StartsWith("No "))
            {
                return "Cannot compare (Current node has no value)";
            }

            int[] idsWhileMoving = new int[10];
            IIntentionNode node = CurrentNode;
            int i = 0;
            while (i < level)
            {
                // Level to high, invalid
                if (node.Parent == null)
                    return "Cannot compare (level too high)";
                idsWhileMoving[i] = node.Parent.Children.IndexOf(node);
                node = node.Parent;
                i++;
            }
            if (node.Parent == null)
                return "Cannot compare (level too high)";
            if (node.Parent.Children.Count <= 1)
                return "Cannot compare (not enough children)";


            compareIDs[level]++;
            // Start over
            if (compareIDs[level] == node.Parent.Children.Count)
                compareIDs[level] = 0;

            node = node.Parent.Children[compareIDs[level]];

            i = level - 1;
            while (i >= 0)
            {
                node = node.Children[idsWhileMoving[i]];
                i--;
            }

            // Repeat comparing because node would compare with itself
            if (CurrentNode == node)
            {
                return Compare(level);
            }

            LastCompareNode = node;
            LastCompareResult = CurrentNode.PathFromRoot + "Value: " + CurrentNode.CalculatedValueFullText +
                                "\n\ncompared to\n\n" +
                                LastCompareNode.PathFromRoot + "Value: " + LastCompareNode.CalculatedValueFullText +
                                "\n\nDifference: " +
                                ((Convert.ToDouble(LastCompareNode.CalculatedValue)/
                                  Convert.ToDouble(CurrentNode.CalculatedValue) - 1)*
                                 100).ToString("F2").Replace(",", ".") + "%";

            LastCompareResultShort = CurrentNode.PathFromRootShort + ", Value: " + CurrentNode.CalculatedValueFullText +
                                     " compared to " +
                                     LastCompareNode.PathFromRootShort + ", Value: " +
                                     LastCompareNode.CalculatedValueFullText +
                                     ", Difference: " +
                                     ((Convert.ToDouble(LastCompareNode.CalculatedValue)/
                                       Convert.ToDouble(CurrentNode.CalculatedValue) - 1)*
                                      100).ToString("F2").Replace(",", ".") + "%";

            return LastCompareResult;
        }

        public IIntentionNode NextNode()
        {
            ResetCompareNodes();

            if (CurrentNode == RootNode)
                return CurrentNode;
            int index = CurrentNode.Parent.Children.IndexOf(CurrentNode);
            if (index < CurrentNode.Parent.Children.Count - 1)
                CurrentNode = CurrentNode.Parent.Children.ElementAt(index + 1);
            return CurrentNode;
        }

        public IIntentionNode PreviousNode()
        {
            ResetCompareNodes();

            if (CurrentNode == RootNode)
                return CurrentNode;
            int index = CurrentNode.Parent.Children.IndexOf(CurrentNode);
            if (index > 0)
                CurrentNode = CurrentNode.Parent.Children.ElementAt(index - 1);
            return CurrentNode;
        }

        public IEnumerable<IIntentionNode> GetNodes(string type, string value)
        {
            return GetNodes(RootNode, type, value);
        }

        public string LastCompareResult { get; private set; }
        public string LastCompareResultShort { get; private set; }

        private IEnumerable<IIntentionNode> GetNodes(IIntentionNode cur, string type, string value)
        {
            if (cur.Value == value && cur.Type == type)
            {
                yield return cur;
            }
            foreach (IIntentionNode child in cur.Children)
            {
                foreach (IIntentionNode node in GetNodes(child, type, value))
                {
                    yield return node;
                }
            }
        }

        private void ResetCompareNodes()
        {
            for (int i = 0; i < compareIDs.Length; i++)
                compareIDs[i] = -1;
            LastCompareNode = null;
        }
    }
}