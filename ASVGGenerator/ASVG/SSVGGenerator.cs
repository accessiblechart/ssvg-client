﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using ASVG;

namespace ASVG
{
    public class ASVGGenerator
    {
        private string csv;

        private char Delimeter = ';';
        private string Options = "";
        private readonly int HeaderRows = -1;
        private readonly int Series = -1;
        private int Color = -1;

        private AggregationType _x1AggregationType = AggregationType.None;
        private AggregationType _x2AggregationType = AggregationType.None;

        private string[] Lines = null;
        private string[][] CsvCells = null;
        private int nRows; // With headings

        public ASVGGenerator(string csv, string options)
        {
            Options = options;
            string[] splitted = Options.Split(';');
            Delimeter =
                Convert.ToChar(splitted.First(o => o.Contains("delimeter")).Split('=').ToArray()[1])
                ;
            splitted =
                splitted.Select(p => p.Trim()).ToArray();

            Series =
                Int32.Parse(
                    splitted
                        .First(o => o.Contains("series"))
                        .Split('=')
                        .Select(x => x.Trim())
                        .ToArray()[1])
                ;

            HeaderRows =
                Int32.Parse(
                    splitted
                        .First(o => o.Contains("headerrows"))
                        .Split('=')
                        .Select(x => x.Trim())
                        .ToArray()[1]);
            /*          Color =
                Int32.Parse(
                    splitted
                        .First(o => o.Contains("color"))
                        .Split('=')
                        .Select(x => x.Trim())
                        .ToArray()[1])
                ;*/

            _x1AggregationType = splitted
                .FirstOrDefault(o => o.Contains("x1AccumulationType")) == null
                ? AggregationType.None
                : splitted
                    .First(o => o.Contains("x1AccumulationType"))
                    .Split('=')
                    .Select(x => x.Trim())
                    .ToArray()[1] == "avg"
                    ? AggregationType.Average
                    : AggregationType.Add;

            _x2AggregationType = splitted
               .FirstOrDefault(o => o.Contains("x2AccumulationType")) == null
               ? AggregationType.None
               : splitted
                   .First(o => o.Contains("x2AccumulationType"))
                   .Split('=')
                   .Select(x => x.Trim())
                   .ToArray()[1] == "avg"
                   ? AggregationType.Average
                   : AggregationType.Add;

            Lines = csv.Replace("\r", "").Split('\n').Select(p => p.Trim()).ToArray();
            CsvCells = new string[Lines.Count()][];

            for (int i = 0; i < Lines.Count(); i++)
            {
                CsvCells[i] = Lines[i].Split(Delimeter).Select(x => x.Trim()).ToArray();
            }

            nRows = Lines.Count(); // With headings
        }

        private DataTable GenerateDataSource()
        {
            string[] Heading =
                Lines[0].Split(Delimeter).Select(p => p.Trim()).ToArray();
            int nColumns = Heading.Count();

            DataSet ds = new DataSet();
            DataTable dt = ds.Tables.Add();
            DataColumn col = null;
            DataRow row = null;

            for (int colIdx = 0; colIdx < nColumns; colIdx++)
            {
                col = new DataColumn(Heading[colIdx], typeof (string));
                col.Caption = col.ColumnName;
                dt.Columns.Add(col);
            }
            col = new DataColumn("RowNumber", typeof (int));
            col.Caption = col.ColumnName;
            dt.Columns.Add(col);

            // Leave out header
            for (int rowIdx = 1; rowIdx < nRows; rowIdx++)
            {
                row = dt.NewRow();
                for (int colIdx = 0; colIdx < nColumns; colIdx++)
                {
                    row[colIdx] = Lines[rowIdx].Split(Delimeter).Select(p => p.Trim()).ToArray()[colIdx];
                }
                // Set rownumber
                row[nColumns] = rowIdx - 1;

                dt.Rows.Add(row);
            }

            return dt;
        }

        /// <summary>
        /// Splits up first by x-Axis, then by series
        /// </summary>
        /// <param name="intentionTree"></param>
        public IIntentionNode GenerateBasicTree(IIntentionTree intentionTree = null)
        {
            if (HeaderRows == 1)
                return GenerateBasicTreeSingleHeadingRow(intentionTree);
            else if (HeaderRows == 2)
                return GenerateBasicTreeDoubleHeadingRow(intentionTree);
            else
                return null;
        }

        private IIntentionNode GenerateBasicTreeDoubleHeadingRow(IIntentionTree intentionTree)
        {
            // Key: x-Entry, Value: List of column numbers of this x-entry
            Dictionary<string, List<int>> x1 = new Dictionary<string, List<int>>();
            // Key: Series name, value: Row number
            Dictionary<string, int> series = new Dictionary<string, int>();

            string[] firstHeaderRow = CsvCells[0];

            for (int i = 0; i < firstHeaderRow.Length; i++)
            {
                if (i == Series)
                    continue;
                if (!string.IsNullOrWhiteSpace(firstHeaderRow[i]))
                {
                    if (!x1.ContainsKey(firstHeaderRow[i]))
                        x1.Add(firstHeaderRow[i], new List<int>());

                    x1[firstHeaderRow[i]].Add(i);
                }
            }

            for (int i = HeaderRows; i < CsvCells.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(CsvCells[i][Series]))
                {
                    series.Add(CsvCells[i][Series], i);
                }
            }

            IIntentionNode root = new IntentionNode(intentionTree, "intentionTree");
            foreach (string x1Entry in x1.Keys)
            {
                IIntentionNode x1Child =
                    root.AddChild(new IntentionNode(intentionTree, CsvCells[0][Series], x1Entry, _x1AggregationType));

                foreach (int x1Column in x1[x1Entry])
                {
                    IIntentionNode x2Child =
                        x1Child.AddChild(new IntentionNode(intentionTree, CsvCells[1][Series], CsvCells[1][x1Column], _x2AggregationType));
                    foreach (KeyValuePair<string, int> seriesPair in series)
                    {
                        IIntentionNode seriesChild =
                            x2Child.AddChild(new IntentionNode(intentionTree, "series",
                                CsvCells[seriesPair.Value][Series]));

                        int colMulti = x1Column > Series ? x1Column - 1 : x1Column;
                        IIntentionNode graphicChild =
                            seriesChild.AddChild(new IntentionNode(intentionTree, "graphic",
                                ((seriesPair.Value - 2)*(CsvCells[0].Length - 1) + colMulti).ToString()));

                    }
                }
            }

            return root;
        }

        private IIntentionNode GenerateBasicTreeSingleHeadingRow(IIntentionTree intentionTree)
        {
            DataTable DataSource = GenerateDataSource();
            int RowNumberCol = DataSource.Columns.Count - 1;
            List<int>
                XAxis =
                    Enumerable.Range(0, DataSource.Columns.Count - 1).Where(i => i != Series).OrderBy(x => x).ToList();

            IIntentionNode root = new IntentionNode(intentionTree, "intentionTree");
            for (int x = 0; x < XAxis.Count; x++)
            {
                IIntentionNode xChild =
                    root.AddChild(new IntentionNode(intentionTree, "x", DataSource.Columns[XAxis[x]].ColumnName, _x1AggregationType));
                for (int row = 0; row < nRows - 1; row++)
                {
                    IIntentionNode seriesChild =
                        xChild.AddChild(new IntentionNode(intentionTree, DataSource.Columns[Series].ColumnName,
                            DataSource.Rows[row][Series].ToString()));

                    IIntentionNode graphicChild =
                        seriesChild.AddChild(new IntentionNode(intentionTree, "graphic",
                            ((int) DataSource.Rows[row][RowNumberCol]*XAxis.Count + x).ToString()));
                }
            }
            return root;
        }

        public static XElement ConvertSNTToXML(IIntentionNode root, XElement parent)
        {
            XElement newNode = root.AsXml;

            if (parent == null)
            {
                parent = newNode;
            }
            else
            {
                parent.Add(newNode);
            }

            foreach (IIntentionNode snChild in root.Children)
            {
                ConvertSNTToXML(snChild, newNode);
            }
            return newNode;
        }

        public static IntentionNode ConvertXMLToSNT(XElement root, IntentionNode parent,
            IntentionTree intentionTree = null)
        {
            IntentionNode newNode = new IntentionNode(intentionTree, root.Name.LocalName, root.Attribute("value").Value,
                root.Attribute("agg") == null ? AggregationType.None : root.Attribute("agg").Value == "avg" ? AggregationType.Average : AggregationType.Add);

            if (parent == null)
            {
                parent = newNode;
            }
            else
            {
                parent.AddChild(newNode);
            }

            foreach (XElement children in root.Elements())
            {
                ConvertXMLToSNT(children, newNode, intentionTree);
            }
            return newNode;
        }
    }
}