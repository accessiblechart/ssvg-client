﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ASVG;

namespace ASVG
{
    public class IntentionNode : IIntentionNode
    {
        public IntentionNode(string type)
        {
            Tree = null;
            Type = type;
            Value = "";
            Children = new List<IIntentionNode>();
            Parent = null;
            AggregationType = AggregationType.None;
        }

        public IntentionNode(string type, string value)
        {
            Tree = null;
            Type = type;
            Value = value;
            Children = new List<IIntentionNode>();
            Parent = null;
            AggregationType = AggregationType.None;
        }

        public IntentionNode(IIntentionTree tree, string type)
        {
            Tree = tree;
            Type = type;
            Value = "";
            Children = new List<IIntentionNode>();
            Parent = null;
            AggregationType = AggregationType.None;
        }

        public IntentionNode(IIntentionTree tree, string type, string value, AggregationType aggregationType = AggregationType.None)
        {
            Tree = tree;
            Type = type;
            Value = value;
            Children = new List<IIntentionNode>();
            Parent = null;
            AggregationType = aggregationType;
        }

        public IIntentionTree Tree { get; set; }
        public string Type { get; private set; }
        public string Value { get; private set; }
        public AggregationType AggregationType { get; private set; }
        public List<IIntentionNode> Children { get; private set; }
        public IIntentionNode Parent { get; set; }

        public IList<IIntentionNode> GraphicChilds
        {
            get
            {
                List<IIntentionNode> graphicChilds = new List<IIntentionNode>();
                graphicChilds.AddRange(Children.Where(x => x.Type == "graphic"));
                // Recursive call
                foreach (IIntentionNode child in Children)
                {
                    graphicChilds.AddRange(child.GraphicChilds);
                }
                return graphicChilds;
            }
        }

        public IList<XElement> GraphicsChildsFromSVG
        {
            get
            {
                if (Tree == null)
                    return null;

                // If SVG-tree is set, select all XML-nodes by comparing the IDs of the GraphicChilds to those of the
                // grapic elements in the SVG
                return
                    GraphicChilds.Select(
                        graphicChildSnNode =>
                            (Tree.SvgXmlRoot.Descendants().SingleOrDefault(ele => ele.Attributes("id").Any() &&
                                                                               ele.Attribute("id").Value ==
                                                                               graphicChildSnNode.Value)))
                        .Where(x => x != null)
                        .ToList();
            }
        }

        public IList<string> GraphicChildIds {
            get
            {
                if (GraphicChilds != null)
                {
                    return GraphicChilds.Select(x => x.Value).ToList();
                }
                return null;
            }
        }

        public string CalculatedValue
        {
            get
            {
                if (GraphicsChildsFromSVG == null)
                    return "No value";
                if (GraphicsChildsFromSVG.Count == 1)
                    return GraphicsChildsFromSVG.Single().Attribute("value").Value;
                if (GraphicsChildsFromSVG.Count > 1)
                {
                    // Calculated value based on aggregation type
                    switch (AggregationType)
                    {
                        case AggregationType.Add:
                            return Children.Select(x => Convert.ToDouble(x.CalculatedValue)).Sum().ToString();
                        case AggregationType.Average:
                            return Children.Select(x => Convert.ToDouble(x.CalculatedValue)).Average().ToString("F2");
                    }
                }
                return "No value";
            }
        }

        public string PathFromRoot
        {
            get
            {
                IIntentionNode cur = this;
                string parent = "";
                int level = 0;
                while (cur != null)
                {
                    level++;
                    cur = cur.Parent;
                }
                level--;
                cur = this;
                while (cur != null)
                {
                    parent = new string('-', level) + new string('-', level) + "> " + cur.Type +
                             (string.IsNullOrEmpty(cur.Value) ? string.Empty : (": " + cur.Value)) + Environment.NewLine +
                             parent;
                    cur = cur.Parent;
                    level--;
                }
                return parent;
            }
        }

        public string PathFromRootShort {
            get
            {
                IIntentionNode cur = this;
                string parent = "";
                while (cur != null)
                {
                    parent = cur.Type +
                             (!string.IsNullOrEmpty(cur.Value) ? (": " + cur.Value) : string.Empty) +
                             (!string.IsNullOrWhiteSpace(parent) ? ", " : string.Empty) +
                             parent;
                    cur = cur.Parent;
                }
                return parent;
            }
        }

        public XElement AsXml
        {
            get
            {
                XElement xml = new XElement(Type);
                xml.SetAttributeValue("value", Value);
                if (AggregationType != AggregationType.None)
                {
                    xml.SetAttributeValue("agg", AggregationType == AggregationType.Average ? "avg" : "add");
                }
                return xml;
            }
        }

        public string CalculatedValueFullText {
            get
            {
                return
                    CalculatedValue + (AggregationType != AggregationType.None
                        ? (" (" + AggregationType + ")")
                        : string.Empty);
            }
        }

        public override string ToString()
        {
            return PathFromRoot;
        }

        public string PathToLeafs(int level)
        {
            string me = new string('-', level) + new string('-', level) + "> " + Type +
                        (string.IsNullOrEmpty(Value) ? string.Empty : (": " + Value)) + Environment.NewLine;
            return Children.Aggregate(me, (current, child) => current + child.PathToLeafs(level + 1));
        }

        public IIntentionNode AddChild(IIntentionNode intentionNode)
        {
            Children.Add(intentionNode);
            intentionNode.Parent = this;
            return intentionNode;
        }

        public IIntentionNode GetChild(int i)
        {
            if (Children.Count > i)
            {
                return Children[i];
            }
            return null;
        }
    }
}