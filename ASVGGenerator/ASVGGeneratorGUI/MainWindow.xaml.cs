﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Navigation;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using MessageBox = System.Windows.MessageBox;
using System.IO;
using ASVG;
using System.Xml.Linq;
using ms = mshtml;

namespace ASVGGeneratorGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private char delimeter = '\t';

        public MainWindow()
        {
            InitializeComponent();
        }


        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                txtSource.Text = ofd.FileName;
            }

        }

        private void btnBrowseDest_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                txtDestFile.Text = ofd.FileName;
            }

        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(txtSource.Text))
            {
                webBrowserSVG.Navigate(new Uri(Directory.GetCurrentDirectory() + @"\SVGGenerate.html"));
            }
            else
            {
                MessageBox.Show("Source file was not found" + Directory.GetCurrentDirectory() + @"\SVGGenerate.html",
                    "Error", MessageBoxButton.OK);
            }
        }


        private void webBrowserSVG_LoadCompleted(object sender, NavigationEventArgs e)
        {
            webBrowserSVG.InvokeScript("drawBarChart",
                new object[] {File.ReadAllText(txtSource.Text), delimeter.ToString()});
            string csv = File.ReadAllText(txtSource.Text, Encoding.UTF8);
            IIntentionTree tree = new IntentionTree(csv,
                "delimeter=" + delimeter + ";series=0; headerrows=2;x1AccumulationType=avg;x2AccumulationType=add");

            ms.HTMLDocument document = (ms.HTMLDocument) webBrowserSVG.Document;
            ms.IHTMLElement svg = document.getElementById("svgcontainer");
            XElement xmlPath = XElement.Parse(svg.outerHTML);
            xmlPath.Add(ASVG.ASVGGenerator.ConvertSNTToXML(tree.RootNode, null));
            string outputPath = txtDestFile.Text;
            xmlPath.Save(outputPath);
        }
    }
}