﻿using System.Windows.Forms;

namespace ASVGViewerGUI
{
    partial class Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowserSVGView = new System.Windows.Forms.WebBrowser();
            this.btnGoUpButton = new System.Windows.Forms.Button();
            this.btnGoDownButton = new System.Windows.Forms.Button();
            this.btnForwardButton = new System.Windows.Forms.Button();
            this.btnBackwardButton = new System.Windows.Forms.Button();
            this.rtbParent = new System.Windows.Forms.RichTextBox();
            this.rtbChild = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rtbValue = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSibling = new System.Windows.Forms.Button();
            this.btnCousin = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnShowValue = new System.Windows.Forms.Button();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.cbBeep = new System.Windows.Forms.CheckBox();
            this.cbTextToSpeech = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rtbHoveredValue = new System.Windows.Forms.RichTextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowserSVGView
            // 
            this.webBrowserSVGView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserSVGView.Location = new System.Drawing.Point(0, 0);
            this.webBrowserSVGView.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserSVGView.Name = "webBrowserSVGView";
            this.webBrowserSVGView.Size = new System.Drawing.Size(1384, 559);
            this.webBrowserSVGView.TabIndex = 0;
            this.webBrowserSVGView.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.DocumentCompleted);
            // 
            // btnGoUpButton
            // 
            this.btnGoUpButton.Enabled = false;
            this.btnGoUpButton.Location = new System.Drawing.Point(90, 19);
            this.btnGoUpButton.Name = "btnGoUpButton";
            this.btnGoUpButton.Size = new System.Drawing.Size(75, 23);
            this.btnGoUpButton.TabIndex = 1;
            this.btnGoUpButton.Text = "Go Up";
            this.btnGoUpButton.UseVisualStyleBackColor = true;
            this.btnGoUpButton.Click += new System.EventHandler(this.GoUpButton_Click);
            // 
            // btnGoDownButton
            // 
            this.btnGoDownButton.Enabled = false;
            this.btnGoDownButton.Location = new System.Drawing.Point(90, 48);
            this.btnGoDownButton.Name = "btnGoDownButton";
            this.btnGoDownButton.Size = new System.Drawing.Size(75, 23);
            this.btnGoDownButton.TabIndex = 2;
            this.btnGoDownButton.Text = "Go Down";
            this.btnGoDownButton.UseVisualStyleBackColor = true;
            this.btnGoDownButton.Click += new System.EventHandler(this.GoDownButton_Click);
            // 
            // btnForwardButton
            // 
            this.btnForwardButton.Enabled = false;
            this.btnForwardButton.Location = new System.Drawing.Point(171, 35);
            this.btnForwardButton.Name = "btnForwardButton";
            this.btnForwardButton.Size = new System.Drawing.Size(75, 23);
            this.btnForwardButton.TabIndex = 3;
            this.btnForwardButton.Text = "Forward";
            this.btnForwardButton.UseVisualStyleBackColor = true;
            this.btnForwardButton.Click += new System.EventHandler(this.ForwardButton_Click);
            // 
            // btnBackwardButton
            // 
            this.btnBackwardButton.Enabled = false;
            this.btnBackwardButton.Location = new System.Drawing.Point(9, 35);
            this.btnBackwardButton.Name = "btnBackwardButton";
            this.btnBackwardButton.Size = new System.Drawing.Size(75, 23);
            this.btnBackwardButton.TabIndex = 4;
            this.btnBackwardButton.Text = "Backward";
            this.btnBackwardButton.UseVisualStyleBackColor = true;
            this.btnBackwardButton.Click += new System.EventHandler(this.BackwardButton_Click);
            // 
            // rtbParent
            // 
            this.rtbParent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbParent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbParent.Location = new System.Drawing.Point(777, 20);
            this.rtbParent.Name = "rtbParent";
            this.rtbParent.Size = new System.Drawing.Size(241, 174);
            this.rtbParent.TabIndex = 5;
            this.rtbParent.Text = "";
            // 
            // rtbChild
            // 
            this.rtbChild.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbChild.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbChild.Location = new System.Drawing.Point(518, 20);
            this.rtbChild.Name = "rtbChild";
            this.rtbChild.Size = new System.Drawing.Size(253, 174);
            this.rtbChild.TabIndex = 6;
            this.rtbChild.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(774, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Parent information:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(512, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Child information:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGoUpButton);
            this.groupBox1.Controls.Add(this.btnGoDownButton);
            this.groupBox1.Controls.Add(this.btnForwardButton);
            this.groupBox1.Controls.Add(this.btnBackwardButton);
            this.groupBox1.Location = new System.Drawing.Point(3, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 79);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Navigation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(264, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Value:";
            // 
            // rtbValue
            // 
            this.rtbValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.rtbValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbValue.Location = new System.Drawing.Point(267, 20);
            this.rtbValue.Name = "rtbValue";
            this.rtbValue.Size = new System.Drawing.Size(245, 174);
            this.rtbValue.TabIndex = 11;
            this.rtbValue.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSibling);
            this.groupBox2.Controls.Add(this.btnCousin);
            this.groupBox2.Location = new System.Drawing.Point(3, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 50);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Compare";
            // 
            // btnSibling
            // 
            this.btnSibling.Enabled = false;
            this.btnSibling.Location = new System.Drawing.Point(6, 19);
            this.btnSibling.Name = "btnSibling";
            this.btnSibling.Size = new System.Drawing.Size(75, 23);
            this.btnSibling.TabIndex = 2;
            this.btnSibling.Text = "Sibling";
            this.btnSibling.UseVisualStyleBackColor = true;
            this.btnSibling.Click += new System.EventHandler(this.Sibling_Click);
            // 
            // btnCousin
            // 
            this.btnCousin.Enabled = false;
            this.btnCousin.Location = new System.Drawing.Point(87, 19);
            this.btnCousin.Name = "btnCousin";
            this.btnCousin.Size = new System.Drawing.Size(75, 23);
            this.btnCousin.TabIndex = 3;
            this.btnCousin.Text = "Cousin";
            this.btnCousin.UseVisualStyleBackColor = true;
            this.btnCousin.Click += new System.EventHandler(this.Cousin_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnShowValue);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 50);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Functions";
            // 
            // btnShowValue
            // 
            this.btnShowValue.Enabled = false;
            this.btnShowValue.Location = new System.Drawing.Point(6, 19);
            this.btnShowValue.Name = "btnShowValue";
            this.btnShowValue.Size = new System.Drawing.Size(75, 23);
            this.btnShowValue.TabIndex = 2;
            this.btnShowValue.Text = "Show Value";
            this.btnShowValue.UseVisualStyleBackColor = true;
            this.btnShowValue.Click += new System.EventHandler(this.ShowValue_Click);
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.webBrowserSVGView);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.cbBeep);
            this.mainSplitContainer.Panel2.Controls.Add(this.cbTextToSpeech);
            this.mainSplitContainer.Panel2.Controls.Add(this.label4);
            this.mainSplitContainer.Panel2.Controls.Add(this.rtbHoveredValue);
            this.mainSplitContainer.Panel2.Controls.Add(this.btnOpenFile);
            this.mainSplitContainer.Panel2.Controls.Add(this.rtbValue);
            this.mainSplitContainer.Panel2.Controls.Add(this.groupBox3);
            this.mainSplitContainer.Panel2.Controls.Add(this.rtbParent);
            this.mainSplitContainer.Panel2.Controls.Add(this.groupBox2);
            this.mainSplitContainer.Panel2.Controls.Add(this.rtbChild);
            this.mainSplitContainer.Panel2.Controls.Add(this.label1);
            this.mainSplitContainer.Panel2.Controls.Add(this.label3);
            this.mainSplitContainer.Panel2.Controls.Add(this.label2);
            this.mainSplitContainer.Panel2.Controls.Add(this.groupBox1);
            this.mainSplitContainer.Size = new System.Drawing.Size(1384, 761);
            this.mainSplitContainer.SplitterDistance = 559;
            this.mainSplitContainer.TabIndex = 12;
            // 
            // cbBeep
            // 
            this.cbBeep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBeep.AutoSize = true;
            this.cbBeep.Location = new System.Drawing.Point(1126, 130);
            this.cbBeep.Name = "cbBeep";
            this.cbBeep.Size = new System.Drawing.Size(51, 17);
            this.cbBeep.TabIndex = 16;
            this.cbBeep.Text = "Beep";
            this.cbBeep.UseVisualStyleBackColor = true;
            // 
            // cbTextToSpeech
            // 
            this.cbTextToSpeech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTextToSpeech.AutoSize = true;
            this.cbTextToSpeech.Checked = true;
            this.cbTextToSpeech.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTextToSpeech.Location = new System.Drawing.Point(1126, 107);
            this.cbTextToSpeech.Name = "cbTextToSpeech";
            this.cbTextToSpeech.Size = new System.Drawing.Size(103, 17);
            this.cbTextToSpeech.TabIndex = 15;
            this.cbTextToSpeech.Text = "Text-To-Speech";
            this.cbTextToSpeech.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1123, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Hovered value:";
            // 
            // rtbHoveredValue
            // 
            this.rtbHoveredValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbHoveredValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbHoveredValue.Location = new System.Drawing.Point(1126, 20);
            this.rtbHoveredValue.Name = "rtbHoveredValue";
            this.rtbHoveredValue.Size = new System.Drawing.Size(246, 81);
            this.rtbHoveredValue.TabIndex = 13;
            this.rtbHoveredValue.Text = "";
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenFile.Location = new System.Drawing.Point(1306, 171);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 12;
            this.btnOpenFile.Text = "Open file";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // Viewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 761);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "Viewer";
            this.Text = "Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Viewer_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            this.mainSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowserSVGView;
        private System.Windows.Forms.Button btnGoUpButton;
        private System.Windows.Forms.Button btnGoDownButton;
        private System.Windows.Forms.Button btnForwardButton;
        private System.Windows.Forms.Button btnBackwardButton;
        private System.Windows.Forms.RichTextBox rtbParent;
        private RichTextBox rtbChild;
        private Label label1;
        private Label label2;
        private GroupBox groupBox1;
        private Label label3;
        private RichTextBox rtbValue;
        private GroupBox groupBox2;
        private Button btnSibling;
        private Button btnCousin;
        private GroupBox groupBox3;
        private Button btnShowValue;
        private SplitContainer mainSplitContainer;
        private Button btnOpenFile;
        private Label label4;
        private RichTextBox rtbHoveredValue;
        private CheckBox cbBeep;
        private CheckBox cbTextToSpeech;
    }
}

