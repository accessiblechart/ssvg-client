﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Forms;
using ASVG;

namespace ASVGViewerGUI
{
    public partial class Viewer : Form
    {
        private static string m_svgText = null;
        private SVGHighlighter m_svgHighlighter = null;
        private IntentionTree m_tree = null;
        private ToolTip m_toolTip;
        private SpeechSynthesizer m_speechSynthesizer;

        #region ctor

        public Viewer()
        {
            InitializeComponent();

            m_toolTip = new ToolTip();
            m_speechSynthesizer = new SpeechSynthesizer();
            m_speechSynthesizer.SetOutputToDefaultAudioDevice();

            var englishVoices = m_speechSynthesizer.GetInstalledVoices(new CultureInfo("en-US"));
            if (englishVoices.Count > 0)
            {
                m_speechSynthesizer.SelectVoice(englishVoices[0].VoiceInfo.Name);
            }
        }

        #endregion ctor

        #region Event handler

        private void GoUpButton_Click(object sender, EventArgs e)
        {
            m_tree.GoUp();
            Highlight(m_tree.CurrentNode.GraphicChildIds);
            rtbValue.Text = m_tree.CurrentNode.CalculatedValueFullText;
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
                m_speechSynthesizer.SpeakAsync(m_tree.CurrentNode.PathFromRootShort);
                m_speechSynthesizer.SpeakAsync("Value: " + m_tree.CurrentNode.CalculatedValueFullText);
                ;
            }
        }

        private void GoDownButton_Click(object sender, EventArgs e)
        {
            m_tree.GoDown(0);
            Highlight(m_tree.CurrentNode.GraphicChildIds);
            rtbValue.Text = m_tree.CurrentNode.CalculatedValueFullText;
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
                m_speechSynthesizer.SpeakAsync(m_tree.CurrentNode.PathFromRootShort);
                m_speechSynthesizer.SpeakAsync("Value: " + m_tree.CurrentNode.CalculatedValueFullText);
                ;
            }
        }

        private void ForwardButton_Click(object sender, EventArgs e)
        {
            m_tree.NextNode();
            Highlight(m_tree.CurrentNode.GraphicChildIds);
            rtbValue.Text = m_tree.CurrentNode.CalculatedValueFullText;
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
                m_speechSynthesizer.SpeakAsync(m_tree.CurrentNode.PathFromRootShort);
                m_speechSynthesizer.SpeakAsync("Value: " + m_tree.CurrentNode.CalculatedValueFullText);
                ;
            }
        }

        private void BackwardButton_Click(object sender, EventArgs e)
        {
            m_tree.PreviousNode();
            Highlight(m_tree.CurrentNode.GraphicChildIds);
            rtbValue.Text = m_tree.CurrentNode.CalculatedValueFullText;
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
                m_speechSynthesizer.SpeakAsync(m_tree.CurrentNode.PathFromRootShort);
                m_speechSynthesizer.SpeakAsync("Value: " + m_tree.CurrentNode.CalculatedValueFullText);
                ;
            }
        }

        private void DocumentCompleted(object sender, EventArgs e)
        {
            rtbParent.Text = m_tree.CurrentNode.PathFromRoot;
            rtbChild.Text = m_tree.CurrentNode.PathToLeafs(0);
        }

        private void Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
            }
        }

        private void WebBrowserDocument_MouseOver(object sender, HtmlElementEventArgs e)
        {
            IWin32Window win = this;
            if (e != null && e.ToElement != null && e.ToElement.TagName == "rect" &&
                !string.IsNullOrEmpty(e.ToElement.GetAttribute("value")) &&
                !string.IsNullOrEmpty(e.ToElement.GetAttribute("id")))
            {
                string id = e.ToElement.GetAttribute("id");
                IIntentionNode hoveredGraphic = m_tree.GetNodes("graphic", id).Single();
                IIntentionNode hoveredNode = hoveredGraphic.Parent;
                string toolTipText = hoveredNode.PathFromRoot + "\n" + "Value: " + hoveredNode.CalculatedValue;
                rtbHoveredValue.Text = hoveredNode.CalculatedValue;
                //Highlight(new[] {id}.ToList());
                m_toolTip.Show(toolTipText, win, PointToClient(MousePosition));
                /*if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
                {
                    m_speechSynthesizer.SpeakAsyncCancelAll();
                    m_speechSynthesizer.SpeakAsync("Hover: " + rtbHoveredValue.Text);
                }*/
            }
            else
            {
                rtbHoveredValue.Text = string.Empty;
                //Highlight(m_tree.CurrentNode.GraphicChildIds);
                m_toolTip.Hide(win);
            }
        }

        private void Sibling_Click(object sender, EventArgs e)
        {
            rtbValue.Text = m_tree.Compare(0);
            if (!rtbValue.Text.StartsWith("Cannot"))
            {
                if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
                {
                    m_speechSynthesizer.SpeakAsyncCancelAll();
                    m_speechSynthesizer.SpeakAsync(m_tree.LastCompareResultShort);
                }

                Highlight(new Dictionary<Color, IList<string>>
                {
                    {Color.Gray, m_tree.LastCompareNode.GraphicChildIds},
                    {Color.Black, m_tree.CurrentNode.GraphicChildIds}
                });

                Beep();
            }
        }

        private void Cousin_Click(object sender, EventArgs e)
        {
            rtbValue.Text = m_tree.Compare(1);

            if (!rtbValue.Text.StartsWith("Cannot"))
            {
                if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
                {
                    m_speechSynthesizer.SpeakAsyncCancelAll();
                    m_speechSynthesizer.SpeakAsync(m_tree.LastCompareResultShort);
                }

                Highlight(new Dictionary<Color, IList<string>>
                {
                    {Color.Gray, m_tree.LastCompareNode.GraphicChildIds},
                    {Color.Black, m_tree.CurrentNode.GraphicChildIds}
                });

                Beep();
            }
        }

        private void ShowValue_Click(object sender, EventArgs e)
        {
            rtbValue.Text = m_tree.CurrentNode.CalculatedValueFullText;
            Highlight(m_tree.CurrentNode.GraphicChildIds);
            if (m_speechSynthesizer != null && cbTextToSpeech.Checked)
            {
                m_speechSynthesizer.SpeakAsyncCancelAll();
                m_speechSynthesizer.SpeakAsync(m_tree.CurrentNode.PathFromRootShort);
                m_speechSynthesizer.SpeakAsync("Value: " + m_tree.CurrentNode.CalculatedValueFullText);
                ;
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult result = ofd.ShowDialog();

            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                if (!string.IsNullOrEmpty(ofd.FileName))
                {
                    m_svgText =
                        File.ReadAllText(ofd.FileName);

                    m_svgHighlighter = new SVGHighlighter(m_svgText);
                    m_tree = new IntentionTree(m_svgText);

                    Highlight(m_tree.CurrentNode.GraphicChildIds);
                    webBrowserSVGView.Document.MouseOver += WebBrowserDocument_MouseOver;

                    btnBackwardButton.Enabled =
                        btnCousin.Enabled =
                            btnForwardButton.Enabled =
                                btnGoDownButton.Enabled =
                                    btnGoUpButton.Enabled = btnShowValue.Enabled = btnSibling.Enabled = true;

                    btnOpenFile.Enabled = false;
                }
            }
        }

        #endregion Event handler

        #region Helper methods

        private void Highlight(IList<string> idsToHighlight)
        {
            if (m_svgHighlighter != null)
            {
                webBrowserSVGView.DocumentText = m_svgHighlighter.Highlight(new Dictionary<Color, IList<string>>
                {
                    {
                        Color.Black, idsToHighlight
                    }
                });
            }
        }

        private void Highlight(IDictionary<Color, IList<string>> idsToHighlight)
        {
            if (m_svgHighlighter != null)
            {
                webBrowserSVGView.DocumentText = m_svgHighlighter.Highlight(idsToHighlight);
            }
        }

        private void Beep()
        {
            if (cbBeep.Checked)
            {
                double highPitch = 1800;

                double max = Convert.ToDouble(m_tree.CurrentNode.CalculatedValue) >
                             Convert.ToDouble(m_tree.LastCompareNode.CalculatedValue)
                    ? Convert.ToDouble(m_tree.CurrentNode.CalculatedValue)
                    : Convert.ToDouble(m_tree.LastCompareNode.CalculatedValue);

                Console.Beep(Convert.ToInt32(Convert.ToDouble(m_tree.CurrentNode.CalculatedValue)/max*highPitch), 1000);
                System.Threading.Thread.Sleep(1000);
                Console.Beep(Convert.ToInt32(Convert.ToDouble(m_tree.LastCompareNode.CalculatedValue)/max*highPitch),
                    1000);
            }
        }

        #endregion Helper methods
    }
}