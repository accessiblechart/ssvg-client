﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ASVGViewerGUI
{
    class SVGHighlighter
    {
        /// <summary>
        /// The ASVG code
        /// </summary>
        string svg_base;

        public SVGHighlighter(string svgText)
        {
            svg_base = svgText;
        }

        /// <summary>
        /// Highlights all graphical elements by drawing a border around them in a given color.
        /// Elements, which should not be highlighted receive a 50% opacity
        /// </summary>
        /// <param name="idGroups">Lists of IDs (graphical elements) which should be highlighted. Key: Highligh color, Value: IDs</param>
        /// <returns>The code for the highlighted SVG</returns>
        public string Highlight(IDictionary<Color,IList<string>> idGroups)
        {
            StringBuilder newsvg = new StringBuilder();
            var lines = svg_base.Replace("/r", "").Split('\n').Select(x => x.Trim()).ToArray();

            // Check all lines in the SVG
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                bool reduceOpacity = true;

                // Check all Color/IDs-Groups
                foreach (KeyValuePair<Color, IList<string>> group in idGroups)
                {
                    foreach (string id in group.Value)
                    {
                        if (line.Contains("id=\"" + id + "\""))
                        {
                            // Draw border if ID should get highlighted
                            line = line.Replace("/>",
                                string.Format("style=\"stroke-width:3;stroke:rgb({0},{1},{2})\" />", group.Key.R,
                                    group.Key.G, group.Key.B));
                            reduceOpacity = false;
                        }
                    }
                }

                // If ID should not get highlighted -> reduce opacity
                if (line.Contains("rect") && reduceOpacity)
                {
                    line = line.Replace("/>", "style=\"fill-opacity:0.5\" />");
                }
                newsvg.Append(line);        
            }

            return newsvg.ToString();
        }
    }
}
