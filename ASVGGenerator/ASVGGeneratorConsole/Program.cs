﻿using System;
using System.IO;
using System.Text;
using System.Xml.Linq;
using ASVG;

namespace ASVGGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(">>Enter csv path (csv with 2 header rows):");
            string path = Console.ReadLine();
            string csv = File.ReadAllText(path, Encoding.UTF8);
            IIntentionTree tree = new IntentionTree(csv, "delimeter=\t;series=0;headerrows=2;x1AccumulationType=avg;x2AccumulationType=add");
            XElement xmlPath = ASVG.ASVGGenerator.ConvertSNTToXML(tree.RootNode, null);
            Console.WriteLine(">>Enter output path:");
            string outputPath = Console.ReadLine();
            if (File.Exists(outputPath))
            {
                if (outputPath.EndsWith(".svg"))
                {
                    XElement rootInOutputFile = XElement.Load(outputPath);
                    rootInOutputFile.Add(xmlPath);
                    File.Delete(outputPath);
                    rootInOutputFile.Save(outputPath);
                    return;
                }
                else
                {
                    File.Delete(outputPath);
                }
            }
            xmlPath.Save(outputPath);
        }
    }
}
